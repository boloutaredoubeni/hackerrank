package com.boloutaredoubeni.hackerrank;

/**
 * Problem Statement
 *
 * Welcome to the world of Java! Just print "Hello World." and "Hello Java." in two separate lines to complete this challenge.
 *
 * The code stub in the editor already creates the main function and Solution class. All you have to do is copy-paste the following lines inside main function.
 *
 * System.out.println("Hello World.");
 * System.out.println("Hello Java.");
 *
 *  Sample Output
 *
 * Hello World.
 * Hello Java.
 *
 */
public class Solution {

    public static void main(String []argv)
    {
        System.out.println("Hello World.");
        System.out.println("Hello Java.");
    }
}
